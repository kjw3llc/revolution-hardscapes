---
title: "About Us"
weight: 3
header_menu: true
---

![Revolution Hardscapes](images/marcusatwork.jpg)

##### Revolution Hardscapes, LLC

A leader in the hardscape industry, specializing in design and installation for customers throughout Virginia and Eastern North Carolina.

We construct patios, driveways, pool decks, walkways, fire pits, outdoor kitchens, seating walls, etc… Our design process is centered on customer input and communication.

Throughout this process, our focus remains on fully understanding the customer’s needs, and ensuring that the customer can visualize exactly how the project will look once completed.

Our extensive installation experience allows us to deliver each customer with a custom project that was designed based on their vision and needs.

Our concentration on customer expectations; combined with our installation capabilities have made us one of the most efficient hardscape companies in the industry.  We look forward to working with you and exceeding your expectations.

Contact us today for a free estimate.

----

Revolution Hardscapes, LLC

Marcus Jones, Owner

(757) 536-1644
