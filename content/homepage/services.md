---
title: "The Services We Offer"
header_menu_title: "Services"
navigation_menu_title: "Services"
weight: 2
header_menu: true
---

This is the important part, right? You want to know what we can do for you.

---

### Driveways

At Revolution Hardscapes, our concern is the customers' confidence and enjoyment of their new driveway.  Our designers offer sleek functional designs, that transform bland concrete deserts into textured and colorful masterpieces.

![Driveway](images/driveway-1-headline.jpg)

We specialize in correcting standing water issues and improving spacial usage throughout our designs.

### Walkways

Our walkways act as a stylish and simple way to change the look of your home to the way you like. Walkways are an easy way to make your home the way that you want.

![Walkway](images/walkway-addon.jpg)

### Pool Decks

Our pool decks are a great and useful transition from land to water. They are an amazing way to make your sparkling pool look even better.

![Pool Deck](images/pool-deck.jpg)

### Patios

Patios are the most popular use for pavers. They are an easy way to make your backyard the dream that you have always wanted.

![Patio](images/patio.png)

Additions to a patio include fire pits and fire places, along with retaining walls and beautiful furniture. Our patios, along with all of our other services are fully customized to your liking.

### Fire Pits and Fire Places

Revolution Hardscapes builds beautiful warm wood burning or gas firepits and fireplaces, fitting them safely into your new or existing landscape.

![Fire Pit](images/firepit.jpeg)

### Retaining Walls

The retaining walls that are installed make for a beautiful way to prevent soil erosion as well as a great way to transition from patio to yard.

![Retaining Wall](images/retaining-wall.jpeg)
